#!/usr/bin/env python

from binance.client import Client
import json
import requests
import argparse
import configparser
import smtplib
from email.mime.text import MIMEText

config = configparser.ConfigParser()
config.read('./config.txt')

api_key = config['KEYPAIR']['APIKEY']
api_secret = config['KEYPAIR']['SECRET']

parser = argparse.ArgumentParser(description='Process some arguments.')

parser.add_argument('--symbol', type=str)
parser.add_argument('--value', type=float)

args = parser.parse_args()

for arg in vars(args):
    argvalue = getattr(args, arg)
    if str(argvalue) == 'None':
        print "No argument given for " + str(arg)
        exec(arg + " = raw_input('Give value for ' + arg + ': ')")
        #print eval(arg)
        if eval(arg) == "":
            print "No value given, exiting, try again.."
            sys.exit(0)
    else:
        exec(arg + " = argvalue")

client = Client(api_key, api_secret)
prices = client.get_all_tickers()
#time_res = client.get_server_time()
#RECV_WINDOW=6000000

def get_price(prices, symbol):
    for price in prices:
        if price['symbol'] == symbol:
            return price['price']
    raise Exception("no price found for {} across the '{}' symbols".format(symbol, len(prices)))

price = get_price(prices, symbol)

if value < price:
    s = smtplib.SMTP('smtp.gmail.com', 587)

    #Next, log in to the server
    emailto = config['GMAIL']['TO']
    emailuser = config['GMAIL']['EMAIL']
    password = config['GMAIL']['PASS']

    s.ehlo()
    s.starttls()
    s.ehlo()

    s.login(emailuser, password)
    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    msg = "Hello!"
    msg = "\r\n".join([
        "From: " + emailuser,
        "To: " + emailto,
        "Subject: ALERT: " + symbol + " < " + str(value) + " (" + str(price) + ")",
        "",
        "The value of " + symbol + " has dropped below " + str(value)
    ])
    s.sendmail(emailuser, emailto, msg)
    s.quit()
